[@ww.label labelKey="Bucket Name" name="propsBucketName"/]
[@ww.label labelKey="File to Upload" name="propsFileToUpload"/]
[@ww.label labelKey="Create Bucket If It Doesn't Exist" name="propsCreateBucketIfNotExist"/]
[@ww.label labelKey="Server-Side Encryption (AES256)" name="propsEncryptFile"/]
