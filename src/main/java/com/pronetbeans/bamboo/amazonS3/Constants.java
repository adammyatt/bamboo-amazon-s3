package com.pronetbeans.bamboo.amazonS3;

/**
 * Constants class.
 * 
 * @author Adam Myatt
 */
public class Constants {

    public static final String PREFIX = "custom.com.pronetbeans.bamboo.amazonS3.";
    public static final String ACCESS_KEY = PREFIX + "awscredentials.accesskey";
    public static final String SECRET_KEY = PREFIX + "awscredentials.secretkey";
    public static final String SERVER_SIDE_ENCRYPTION = "AES256";
    
    public static String[] banner = {
        "______          _   _      _  ______  ",
        "| ___ \\        | \\ | |    | | | ___ \\ ",
        "| |_/ / __ ___ |  \\| | ___| |_| |_/ / ___  __ _ _ __  ___   ___ ___  _ __ ___ ",
        "|  __/ '__/ _ \\| . ` |/ _ \\ __| ___ \\/ _ \\/ _` | '_ \\/ __| / __/ _ \\| '_ ` _ \\ ",
        "| |  | | | (_) | |\\  |  __/ |_| |_/ /  __/ (_| | | | \\__ \\| (_| (_) | | | | | |",
        "\\_|  |_|  \\___/\\_| \\_/\\___|\\__\\____/ \\___|\\__,_|_| |_|___(_)___\\___/|_| |_| |_|"
    };
}
