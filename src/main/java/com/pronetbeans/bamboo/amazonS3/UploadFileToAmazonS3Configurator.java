package com.pronetbeans.bamboo.amazonS3;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Class for configuring parameters used by class for uploading file from Amazon
 * S3 bucket.
 *
 * @author Adam Myatt
 */
public class UploadFileToAmazonS3Configurator extends AbstractTaskConfigurator {

    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("propsBucketName", params.getString("propsBucketName"));
        config.put("propsFileToUpload", params.getString("propsFileToUpload"));
        config.put("propsCreateBucketIfNotExist", params.getString("propsCreateBucketIfNotExist"));
        config.put("propsEncryptFile", params.getString("propsEncryptFile"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        context.put("propsBucketName", taskDefinition.getConfiguration().get("propsBucketName"));
        context.put("propsFileToUpload", taskDefinition.getConfiguration().get("propsFileToUpload"));
        context.put("propsCreateBucketIfNotExist", taskDefinition.getConfiguration().get("propsCreateBucketIfNotExist"));
        context.put("propsEncryptFile", taskDefinition.getConfiguration().get("propsEncryptFile"));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        context.put("propsBucketName", taskDefinition.getConfiguration().get("propsBucketName"));
        context.put("propsFileToUpload", taskDefinition.getConfiguration().get("propsFileToUpload"));
        context.put("propsCreateBucketIfNotExist", taskDefinition.getConfiguration().get("propsCreateBucketIfNotExist"));
        context.put("propsEncryptFile", taskDefinition.getConfiguration().get("propsEncryptFile"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        final String propsBucketName = params.getString("propsBucketName");
        if (StringUtils.isEmpty(propsBucketName)) {
            errorCollection.addError("propsBucketName", textProvider.getText("com.pronetbeans.bamboo.amazonS3.UploadFileToAmazonS3Task.error"));
        }
        final String propsFileToUpload = params.getString("propsFileToUpload");
        if (StringUtils.isEmpty(propsFileToUpload)) {
            errorCollection.addError("propsFileToUpload", textProvider.getText("com.pronetbeans.bamboo.amazonS3.UploadFileToAmazonS3Task.error"));
        }
    }

    public void setTextProvider(final TextProvider textProvider) {
        this.textProvider = textProvider;
    }
}
