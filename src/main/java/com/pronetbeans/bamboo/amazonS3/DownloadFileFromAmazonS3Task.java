package com.pronetbeans.bamboo.amazonS3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.security.StringEncrypter;
import com.atlassian.bamboo.task.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * Task for downloading a file from an Amazon S3 bucket.
 *
 * @author Adam Myatt
 */
public class DownloadFileFromAmazonS3Task implements TaskType {

    private static final Logger log = Logger.getLogger(DownloadFileFromAmazonS3Task.class);

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {

        final BuildLogger buildLogger = taskContext.getBuildLogger();

        Map<String, String> custom = taskContext.getBuildContext().getParentBuildContext().getBuildDefinition().getCustomConfiguration();

        StringEncrypter encrypter = new StringEncrypter();

        final String propsAccessKey = encrypter.decrypt(custom.get(Constants.ACCESS_KEY));
        final String propsSecretKey = encrypter.decrypt(custom.get(Constants.SECRET_KEY));
        final String propsBucketName = taskContext.getConfigurationMap().get("propsBucketName");
        final String propsFileToDownload = taskContext.getConfigurationMap().get("propsFileToDownload");
        final String propsLocalFileName = taskContext.getConfigurationMap().get("propsLocalFileName");

        buildLogger.addBuildLogEntry("Bucket Name : " + propsBucketName);
        buildLogger.addBuildLogEntry("File To Download : " + propsFileToDownload);
        buildLogger.addBuildLogEntry("Local File Name : " + propsLocalFileName);

        boolean itWorked = false;

        try {
            CustomPropertiesCredentials cpc = new CustomPropertiesCredentials();
            cpc.setAWSAccessKeyId(propsAccessKey);
            cpc.setSecretAccessKey(propsSecretKey);

            final AmazonS3 s3 = new AmazonS3Client(cpc);

            S3Object object = s3.getObject(new GetObjectRequest(propsBucketName, propsFileToDownload));
            
            buildLogger.addBuildLogEntry("Downloaded Object ContentType : " + object.getObjectMetadata().getContentType());

            byte[] buffer = new byte[8 * 1024];

            InputStream input = object.getObjectContent();
            File localFile = new File(taskContext.getWorkingDirectory(), propsLocalFileName);

            try {
                OutputStream output = new FileOutputStream(localFile);
                try {
                    int bytesRead;

                    while ((bytesRead = input.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } finally {

                    try {
                        if (output != null) {
                            output.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } finally {

                try {
                    if (input != null) {
                        input.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            itWorked = true;

            log.info(Utils.getLogBanner());

        } catch (Exception e) {
            buildLogger.addBuildLogEntry("Error downloading file from Amazon S3 Bucket : " + e.getMessage());
            e.printStackTrace();
        }

        if (itWorked) {
            return TaskResultBuilder.create(taskContext).success().build();
        } else {
            return TaskResultBuilder.create(taskContext).failedWithError().build();
        }
    }
}