package com.pronetbeans.bamboo.amazonS3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.security.StringEncrypter;
import com.atlassian.bamboo.task.*;
import com.atlassian.bamboo.utils.FileVisitor;
import java.io.File;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;
import org.jetbrains.annotations.NotNull;

/**
 * Bamboo Task for uploading file.
 *
 * @author Adam Myatt
 */
public class UploadFileToAmazonS3Task implements TaskType {

    private static final Logger log = Logger.getLogger(UploadFileToAmazonS3Task.class);

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {

        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.create(taskContext);
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        Map<String, String> custom = taskContext.getBuildContext().getParentBuildContext().getBuildDefinition().getCustomConfiguration();

        StringEncrypter encrypter = new StringEncrypter();

        String encryptedAccessKey = custom.get(Constants.ACCESS_KEY);
        String encryptedSecretKey = custom.get(Constants.SECRET_KEY);

        String propsAccessKey = null;
        String propsSecretKey = null;

        try {
            propsAccessKey = encrypter.decrypt(encryptedAccessKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            propsSecretKey = encrypter.decrypt(encryptedSecretKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final String propsBucketName = taskContext.getConfigurationMap().get("propsBucketName");
        final String propsFileToUpload = taskContext.getConfigurationMap().get("propsFileToUpload");
        final String propsCreateBucketIfNotExist = taskContext.getConfigurationMap().get("propsCreateBucketIfNotExist");
        final String propsEncryptFile = taskContext.getConfigurationMap().get("propsEncryptFile");
        boolean itWorked = false;

        try {
            CustomPropertiesCredentials cpc = new CustomPropertiesCredentials();
            cpc.setAWSAccessKeyId(propsAccessKey);
            cpc.setSecretAccessKey(propsSecretKey);
            final AmazonS3 s3 = new AmazonS3Client(cpc);

            final File workingDir = taskContext.getWorkingDirectory();
            final FileVisitor visitor = new FileVisitor(workingDir) {

                @Override
                public void visitFile(final File file) throws InterruptedException {
                    if (file.isFile()) {
                        try {
                            final String filePath = file.getAbsolutePath();

                            File fileToCopy = new File(filePath);

                            buildLogger.addBuildLogEntry("Preparing to load file : " + propsFileToUpload + " to S3 Bucket : " + propsBucketName);

                            if (s3.doesBucketExist(propsBucketName)) {


                                PutObjectRequest put = new PutObjectRequest(propsBucketName, fileToCopy.getName(), fileToCopy);

                                if ("true".equals(propsEncryptFile)) {
                                    ObjectMetadata metadata = new ObjectMetadata();
                                    metadata.setServerSideEncryption(Constants.SERVER_SIDE_ENCRYPTION);
                                    put.setMetadata(metadata);
                                }
                                s3.putObject(put);

                            } else {

                                if ("true".equals(propsCreateBucketIfNotExist)) {

                                    Bucket buck = s3.createBucket(propsBucketName);

                                    if (buck != null) {
                                        PutObjectRequest put = new PutObjectRequest(propsBucketName, fileToCopy.getName(), fileToCopy);

                                        if ("true".equals(propsEncryptFile)) {
                                            ObjectMetadata metadata = new ObjectMetadata();
                                            metadata.setServerSideEncryption(Constants.SERVER_SIDE_ENCRYPTION);
                                            put.setMetadata(metadata);
                                        }
                                        s3.putObject(put);
                                        buildLogger.addBuildLogEntry("Created Bucket " + propsBucketName + ".");
                                    } else {
                                        throw new InterruptedException("Failed to create bucket with name '" + propsBucketName + "'");
                                    }
                                } else {
                                    throw new InterruptedException("Failed to upload file as the bucket with name '" + propsBucketName + "' does not exist and you did not specify to create it otherwise.");
                                }
                            }

                            buildLogger.addBuildLogEntry("File Upload to Bucket " + propsBucketName + "Complete.");

                        } catch (Exception e) {
                            buildLogger.addErrorLogEntry("Failed to upload file '" + file.getAbsolutePath() + "'", e);
                            throw new InterruptedException(e.getLocalizedMessage());
                        }
                    }
                }
            };

            FileSet fileSet = new FileSet();
            fileSet.setDir(workingDir);

            PatternSet.NameEntry include = fileSet.createInclude();
            include.setName(propsFileToUpload);

            DirectoryScanner ds = fileSet.getDirectoryScanner(new Project());
            String[] srcFiles = ds.getIncludedFiles();

            if (srcFiles == null || srcFiles.length == 0) {
                // the file specified is not found in the build directory
                buildLogger.addBuildLogEntry("The file specified is not found in the build directory.");
                itWorked = false;
            } else {
                try {
                    visitor.visitFilesThatMatch(propsFileToUpload);
                    itWorked = true;
                } catch (InterruptedException e) {
                    buildLogger.addErrorLogEntry("Failed to upload file.", e);
                    taskResultBuilder.failedWithError().build();
                    itWorked = false;
                }
            }

            log.info(Utils.getLogBanner());

        } catch (Exception e) {
            buildLogger.addErrorLogEntry("Error loading file to Amazon S3 Bucket : " + e.getMessage(), e);
            e.printStackTrace();
        }

        if (itWorked) {
            return TaskResultBuilder.create(taskContext).success().build();
        } else {
            return TaskResultBuilder.create(taskContext).failedWithError().build();
        }
    }
}